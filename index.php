 	<!DOCTYPE html>
<!--[if IE 8]> 				 <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<html>
	<head>
		<meta charset="utf-8" />
  	<meta name="viewport" content="width=device-width" />

  	<title>Sub Geolocator</title>

		<link rel="stylesheet" type="text/css" href="css/foundation.min.css">
		<link rel="stylesheet" type="text/css" href="css/normalize.css">
		<link rel="stylesheet" type="text/css" href="css/style.css">

		<script src="Javascripts/vendor/custom.modernizr.js"></script>
	</head>
	<body id="body">
		<div class="row">
			<div class="large-8 small-centered columns">
				<h2 class="title">Sub Geolocator</h2>
			</div>
		</div>

		<div id="map-canvas">
		</div>

		<div class="row">
			<div class="options large-6 small-centered columns">
				<div class="small-12">
					<button id="save" class="small radius button expand">Save</button>
				</div>
				<div class="small-12">
					<button id="find" class="small radius button expand">Find Me</button>
				</div>
				<div class="row">
					<div class="small-5 columns">
						<div class="row">
							<div class="small-3 columns">
								<img class="showIcon" src="img/subway.png"/>
							</div>
							<div class="small-9 columns">
								<h5>Subway</h5>
							</div>
						</div>
					</div>
					<div class="small-7 columns">
						<button id="subwayShow" class="small radius button">Hide</button>
					</div>
				</div>
				<div class="row">
					<div class="small-5 columns">
						<div class="row">
							<div class="small-3 columns">
								<img class="showIcon" src="img/mrsub.png"/>
							</div>
							<div class="small-9 columns">
								<h5>Mr Sub</h5>
							</div>
						</div>
					</div>
					<div class="small-7 columns">
						<button id="mrsubShow" class="small radius button">Hide</button>
					</div>
				</div>
				<div class="row">
					<div class="small-5 columns">
						<div class="row">
							<div class="small-3 columns">
								<img class="showIcon" src="img/quiznos.png"/>
							</div>
							<div class="small-9 columns">
								<h5>Quiznos</h5>
							</div>
						</div>
					</div>
					<div class="small-7 columns">
						<button id="quiznosShow" class="small radius button">Hide</button>
					</div>
				</div>
				<!--<input id="addInputAddress" />
				<input id="addInputCategory" />
				<button id="addButton" class="small radius button info">Add</button>-->

			</div>
		</div>

		<div id="myModal" class="reveal-modal large">
		  <h2 style="text-align:center;">Edit Current Marker</h2>
	  	<div class="row">
	  		<div class="small-3 columns">
	  			<label class="right inline">Title:</label>
	  		</div>
	  		<div class="small-9 columns">
	  			<textarea id="markerTitleModal" class="twelve" name="title" type="text" placeholder="Marker Title"></textarea>
	  		</div>
	  	</div>
	  	</br>
	  	<div class="row">
	  		<div class="small-3 columns">
	  			<label class="right inline">Address:</label>
	  		</div>
	  		<div class="small-9 columns">
	  			<textarea id="markerAddressModal" class="twelve" name="address" type="text" placeholder="Marker Address"></textarea>
	  		</div>
	  	</div>
	  	</br>
	  	<div class="row">
	  		<div class="small-3 columns">
	  			<label class="right inline">Category:</label>
	  		</div>
	  		<div class="small-9 columns">
	  			<textarea id="markerCategoryModal" class="twelve" name="category" type="text" placeholder="Marker Category"></textarea>
	  		</div>
	  	</div>
	  	</br>
	  	<input id="markerIdModal" type="hidden"></textarea>
	  	<div class="row">
        <div class="small-3 small-offset-3 columns">
          <button class="saveButton button success radius small">Save Changes</button>
        </div>
      </div>
		  <a class="close-reveal-modal">&#215;</a>
		</div>

		<script src="Javascripts/vendor/jquery.js"></script>
	  <script src="Javascripts/foundation.min.js"></script>

	  <script>
	    $(document).foundation();
	  </script>
	  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCSCr4BeNnu-zBlagFmT3k66laCn7Xneio&sensor=true">
    </script>
    <script type="text/javascript">
    	$(document).ready(function(){
      	var markersArray = [];
      	var geocoder = new google.maps.Geocoder();
      	var currentMarker;
      	var northCoord, westCoord, southCoord, eastCoord;
      	var bounds, northEast, southWest;
      	var subwayIsHidden = false, mrsubIsHidden = false, quiznosIsHidden = false;

      	var subwayImage = {
      		url: 'img/subway.png',
		   		size: new google.maps.Size(32, 37),
		    	origin: new google.maps.Point(0, 0),
		    	anchor: new google.maps.Point(16, 37)
		    };

		    var mrsubImage = {
      		url: 'img/mrsub.png',
		   		size: new google.maps.Size(32, 37),
		    	origin: new google.maps.Point(0, 0),
		    	anchor: new google.maps.Point(16, 37)
		    };

		    var quiznosImage = {
      		url: 'img/quiznos.png',
		   		size: new google.maps.Size(32, 37),
		    	origin: new google.maps.Point(0, 0),
		    	anchor: new google.maps.Point(16, 37)
		    };

		    var noCategoryImage = {
      		url: 'img/nocategory.png',
		   		size: new google.maps.Size(32, 37),
		    	origin: new google.maps.Point(0, 0),
		    	anchor: new google.maps.Point(16, 37)
		    };

		    var hereImage = {
      		url: 'img/here.png',
		   		size: new google.maps.Size(32, 37),
		    	origin: new google.maps.Point(0, 0),
		    	anchor: new google.maps.Point(16, 37)
		    };

        var mapOptions = {
          center: new google.maps.LatLng(42.948881, -81.24862),
          zoom: 11,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          zoomControl: true
        };
        var map = new google.maps.Map(document.getElementById("map-canvas"),
            mapOptions);

        var infowindow = new google.maps.InfoWindow( {
				  	content: "",
		        size: new google.maps.Size(80,80)
		    });

        var markers = <?php require_once("load.php"); ?>;

        if(markers != null) {
	        for(i in markers){
	        	var location = new google.maps.LatLng(markers[i].position.jb, markers[i].position.kb);
	        	placeMarker(location, markers[i].title, markers[i].address, markers[i].category);
	        	if(i == 0) {
	        		northCoord = markers[i].position.jb;
	        		southCoord = markers[i].position.jb;
	        		westCoord = markers[i].position.kb;
	        		eastCoord = markers[i].position.kb;
	        	}
	        	else {
		        	if(markers[i].position.jb > northCoord)
		        		northCoord = markers[i].position.jb;
		       		if(markers[i].position.jb < southCoord)
		       			southCoord = markers[i].position.jb;
		       		if(markers[i].position.kb < westCoord)
		       			westCoord = markers[i].position.kb;
		       		if(markers[i].position.kb > eastCoord)
		       			eastCoord = markers[i].position.kb;
		       	}
	        }

	        northEast = new google.maps.LatLng(northCoord, eastCoord);
		      southWest = new google.maps.LatLng(southCoord, westCoord);
		      bounds = new google.maps.LatLngBounds(southWest, northEast);
		      map.fitBounds(bounds);
	      }

	      placeMyLocation();

			  function placeMarker(location, title, address, category) {
				  var marker = new google.maps.Marker({
			      position: location,
			      icon: "",
			      draggable: true,
			      title: title,
			      category: category,
			      animation: google.maps.Animation.DROP,
			      map: map,
			      address: address,
			      id: markersArray.length
				  });

				 	getIcon(marker);

				 	if(marker.address === "") {
					  getAddress(marker);
					}

				  google.maps.event.addListener(marker, 'click', function() {
				  	infowindow.setContent("<p id='" + marker.id + "_address' class='address'>" + marker.address + 
				  		'</p></br><button class="edit small radius button">Edit</button>' +
				  		'<button class="zoom small radius button">Zoom In</button>');
				  	infowindow.open(map, marker);
				  });

				  google.maps.event.addListener(marker, 'dragend', function() {
				  	saveMarkers();
				  });

				  markersArray.push(marker);
				}

				function saveMarkers() {
	      	if(markersArray) {
	      		jsonData = {};
	      		for(i in markersArray) {
	      			jsonData[i] = {"title": markersArray[i].title, "position": markersArray[i].position,
	      			"address": markersArray[i].address, "category": markersArray[i].category};
	      		}

	      		jsonString = JSON.stringify(jsonData);

	      		$.ajax({
	      			type: 'POST',
	      			url: "save.php",
	      			data: {save: jsonString},
	      			success: function(data) {

	      				$('#map-canvas').hide();
	      				$('#map-canvas').fadeIn('slow');
	      			}
	      		});
	      	}
	      }

	      function getIcon(marker) {
	      	if(marker.category.toLowerCase() == "subway")
	      		marker.setIcon(subwayImage);	
	      	else if(marker.category.toLowerCase() == "mrsub")
	      		marker.setIcon(mrsubImage);
	      	else if(marker.category.toLowerCase() == "quiznos")
	      		marker.setIcon(quiznosImage);
	      	else if(marker.category.toLowerCase() == "here")
	      		marker.setIcon(hereImage);
	      	else
	      		marker.setIcon(noCategoryImage);
	      }

	      function getAddress(marker) {
	      	geocoder.geocode({'latLng': marker.position}, function(results, status) {
	      		if(status == google.maps.GeocoderStatus.OK) {
	      			marker.address = results[0].formatted_address;
	      			if(marker.title === "")
	      				marker.setTitle(results[0].formatted_address);
	      		}
	      		else
	      			marker.address = "Failed - " + status;
	      	});
	      }

	      function addMarkerByAddress(address, category) {
	      	geocoder.geocode({'address': address}, function(results, status) {
	      		if(status == google.maps.GeocoderStatus.OK) {
	      			placeMarker(results[0].geometry.location, address, address, category);
	      		}
	      	});
	      }

	      function checkBounds() {
	      	var initialSet = false;
	      	for(i in markers){
	        	if(subwayIsHidden == true && markers[i].category == "subway") {
	        	}
	        	else if(mrsubIsHidden == true && markers[i].category == "mrsub") {
	        	}
	        	else if(quiznosIsHidden == true && markers[i].category == "quiznos") {
	        	}
	        	else {
		        	if(initialSet == false) {
		        		northCoord = markers[i].position.jb;
		        		southCoord = markers[i].position.jb;
		        		westCoord = markers[i].position.kb;
		        		eastCoord = markers[i].position.kb;
		        		initialSet = true;
		        	}
		        	else {
			        	if(markers[i].position.jb > northCoord)
			        		northCoord = markers[i].position.jb;
			       		if(markers[i].position.jb < southCoord)
			       			southCoord = markers[i].position.jb;
			       		if(markers[i].position.kb < westCoord)
			       			westCoord = markers[i].position.kb;
			       		if(markers[i].position.kb > eastCoord)
			       			eastCoord = markers[i].position.kb;
			       	}
		       	}
	        }

	        northEast = new google.maps.LatLng(northCoord, eastCoord);
		      southWest = new google.maps.LatLng(southCoord, westCoord);
		      bounds = new google.maps.LatLngBounds(southWest, northEast);
		      map.fitBounds(bounds);
	      }

	      function clearMarkers() {
	    		if (markersArray) {
					  for (i in markersArray) {
					    markersArray[i].setMap(null);
					  }

					  markersArray.length = 0;
					}
	    	}

	    	function showMarkers(category) {
	    		if (markersArray) {
					  for (i in markersArray) {
					  	if(markersArray[i].category === category)
					    	markersArray[i].setMap(map);
					  }
					}
	    	}

	    	function hideMarkers(category) {
	    		if (markersArray) {
					  for (i in markersArray) {
					  	if(markersArray[i].category === category)
					    	markersArray[i].setMap(null);
					  }
					}
	    	}

	    	function placeMyLocation() {
	    		if(navigator.geolocation) {
	    			var currentLocation = navigator.geolocation.getCurrentPosition(placeLocation);	
	    		}
	    	}

	    	function placeLocation(position) {
	    		var coords = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
	    		placeMarker(coords, "You are here", "", "here");
	    	}

	    	function findMyLocation() {
	    		if(navigator.geolocation) {
	    			var currentLocation = navigator.geolocation.getCurrentPosition(showLocation);	
	    		}
	    	}

	    	function showLocation(position) {
	    		var coords = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
	    		map.setCenter(coords);
	    		map.setZoom(12);
	    	}

	    	$('body').on('click', '.edit', function() {
	    		$address = $(this).parent().find('.address');
	    		var id = parseInt($address[0].id);
	    		currentMarker = markersArray[id];

	    		$('#markerTitleModal').val(currentMarker.title);
	    		$('#markerAddressModal').val(currentMarker.address);
	    		$('#markerCategoryModal').val(currentMarker.category);
	    		$('#markerIdModal').val(id);

	    		$('#myModal').foundation('reveal', 'open');
	    	});

	    	$('body').on('click', '.saveButton', function() {
	    		$('#myModal').foundation('reveal', 'close');
	    		var id = $('#markerIdModal').val();
	    		markersArray[id].title = $('#markerTitleModal').val();
	    		currentMarker.setTitle($('#markerTitleModal').val());
	    		markersArray[id].address = $('#markerAddressModal').val();

	    		markersArray[id].category = $('#markerCategoryModal').val();	    		
	    		getIcon(currentMarker);

	    		infowindow.setContent("<p id='" + id + "_address' class='address'>" + $('#markerAddressModal').val() + 
	    			'</p></br><button class="edit small radius button info">Edit</button>' +
	    			'<button class="zoom small radius button">Zoom In</button>');

	    		saveMarkers();
	    	});

	    	$('body').on('click', '#addButton', function() {
	    		addMarkerByAddress($('#addInputAddress').val(), $('#addInputCategory').val());
	    	});

	    	$('body').on('click', '.zoom', function() {
	    		$address = $(this).parent().find('.address');
	    		var id = parseInt($address[0].id);
	    		currentMarker = markersArray[id];

	    		map.setCenter(currentMarker.getPosition());
	    		map.setZoom(16);
	    	})

	    	$('#subwayShow').click(function(){
	    		if($(this).text() === "Hide") {
	    			$(this).text("Show");
	    			subwayIsHidden = true;
	    			hideMarkers("subway");
	    		}
	    		else {
	    			$(this).text("Hide");
	    			subwayIsHidden = false;
	    			showMarkers("subway");
	    		}
	    		checkBounds();
	    	});

	    	$('#mrsubShow').click(function(){
	    		if($(this).text() === "Hide") {
	    			$(this).text("Show");
	    			mrsubIsHidden = true;
	    			hideMarkers("mrsub");
	    		}
	    		else {
	    			$(this).text("Hide");
	    			mrsubIsHidden = false;
	    			showMarkers("mrsub");
	    		}
	    		checkBounds();
	    	});

	    	$('#quiznosShow').click(function(){
	    		if($(this).text() === "Hide") {
	    			$(this).text("Show");
	    			quiznosIsHidden = true;
	    			hideMarkers("quiznos");
	    		}
	    		else {
	    			$(this).text("Hide");
	    			quiznosIsHidden = false;
	    			showMarkers("quiznos");
	    		}
	    		checkBounds();
	    	});

	      document.getElementById("save").addEventListener('click', function() {
	      	saveMarkers();
	      });

	      document.getElementById("find").addEventListener('click', function() {
	      	findMyLocation();
	      });
      });
    </script>
	</body>
</html>